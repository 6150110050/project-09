package com.example.login;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static android.Manifest.permission.CAMERA;

public class MemberDetail extends AppCompatActivity {

    private static final int REQUEST_CAMERA = 100;
    private static String[] PERMISSIONS_CAMERA = {CAMERA};
    ImageView  img2,img3 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);


        img2 = findViewById(R.id.scanqr);
        img3 = findViewById(R.id.profile);

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberDetail.this,ScanQR.class);
                startActivity(intent);
            }
        });


        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberDetail.this,Profile.class);
                startActivity(intent);
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                    if (checkPermission()) {
                        Toast.makeText(getApplicationContext(), "Permission already granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        requestPermission(MemberDetail.this);
                    }
                }
            }
        });
    }
    private boolean checkPermission() {
        // Check if we have permission
        return (ContextCompat.checkSelfPermission(
                getApplicationContext(),
                CAMERA) == PackageManager.PERMISSION_GRANTED
        );
    }
    private void requestPermission(Activity activity) {
        // We don't have permission so prompt the user
        ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_CAMERA,
                REQUEST_CAMERA
        );
    }
}